# Copyright 2017 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

CMAKE_SOURCE=${WORK}-src

require cmake [ api=2 ]

SUMMARY="MySQL database connector for C++ (mimics JDBC 4.0 API)"
HOMEPAGE="http://dev.mysql.com/downloads/connector/cpp/"
DOWNLOADS="https://dev.mysql.com/get/Downloads/Connector-C++/${PNV}-src.tar.gz"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-libs/boost
    build+run:
        dev-db/mysql
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

BUGS_TO="hasufell@posteo.de"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-Fix-build-with-LibreSSL.patch
    "${FILES}"/0002-Don-t-call-ar-directly-use-CMAKE_AR.patch
)

src_configure() {
    CMAKE_SRC_CONFIGURE_PARAMS=(
        -DCMAKE_BUILD_TYPE=Release
        -DINSTALL_DOC_DIR="/usr/share/doc/${PNVR}"
        -DINSTALL_LIB_DIR="lib"
        -DWITH_JDBC=ON
    )

    cmake_src_configure
}

src_prepare() {
    cmake_src_prepare
}

src_install() {
    cmake_src_install
    edo rm "${IMAGE}"/usr/$(exhost --target)/BUILDINFO.txt
}
