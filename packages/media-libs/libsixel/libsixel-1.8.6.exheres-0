# Copyright 2020 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user="saitoha" release="v${PV}" suffix="tar.gz" ]
require bash-completion zsh-completion

SUMMARY="A lightweight, fast implementation of DEC SIXEL graphics codec"

LICENCES="MIT public-domain"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/glib:2
        media-libs/gd
        media-libs/libpng:=
        net-misc/curl
        sys-libs/zlib
        x11-libs/gdk-pixbuf:2.0
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

BUGS_TO="hasufell@posteo.de"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-python
    --with-bash-completion-dir=${BASHCOMPLETIONDIR}
    --with-curl
    --with-gd
    --with-gdk-pixbuf2
    --with-jpeg
    --with-png
    --with-zshcompletiondir=${ZSHCOMPLETIONDIR}
    )

WORK=${WORKBASE}/sixel-${PV}

src_install() {
    default
    emagicdocs
}

