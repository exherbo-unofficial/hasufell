# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2
#
# Based in part upon 'openrc-0.20.4.ebuild' from Gentoo, which is:
#     Copyright 1999-2015 Gentoo Foundation
#     Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion
require github

SUMMARY="A dependency-based init system that works with the system-provided init program"
SLOT="0"
LICENCES="BSD-2"

MYOPTIONS="
    ( providers:
        runit
        sinit
        sysvinit
    ) [[
        *description = [ provider for the init daemon ]
        number-selected = at-most-one
    ]]
    ncurses
    newnet [[ description = [
            Enable the new network stack (experimental)
        ] ]]
    pam
"

PLATFORMS="~amd64 ~x86"

DEPENDENCIES="
    build+run:
        ncurses? ( sys-libs/ncurses )
        pam? ( sys-libs/pam )
    run:
        app-shells/bash
        providers:runit? ( sys-apps/runit )
        providers:sinit? ( sys-apps/sinit )
        providers:sysvinit? ( sys-apps/sysvinit )
    recommendation:
        net-misc/netifrc [[ description = [
            Network Interface Management Scripts
        ] ]]
"

REMOTE_IDS="github:OpenRC/${PN}"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/src-rc-rc-logger.h-fix-build-failure-against-gcc-10.patch
)

DEFAULT_SRC_COMPILE_PARAMS=(
    LIBNAME=lib
    LIBEXECDIR=/usr/$(exhost --target)/libexec/rc
    OS=Linux
)

DEFAULT_SRC_INSTALL_PARAMS=(
    PREFIX=/usr/$(exhost --target)
    SBINDIR=/usr/$(exhost --target)/bin
    SYSCONFDIR=/etc
    UPREFIX=/usr
    INCDIR=/usr/$(exhost --target)/include
    INCMODE=0755
    LIBDIR=/usr/$(exhost --target)/lib
    SHLIBDIR=/usr/$(exhost --target)/lib
    LIBMODE=0755
    BASHCOMPDIR=${BASHCOMPLETIONDIR}
    ZSHCOMPDIR=${ZSHCOMPLETIONDIR}
)

pkg_setup() {
    exdirectory --allow /etc/sysctl.d
}

src_compile() {
    export BRANDING="Exherbo Linux"

    export DEBUG=
    export MKTERMCAP=$(optionv ncurses)

    export AR=$(exhost --tool-prefix)ar
    export RANLIB=$(exhost --tool-prefix)ranlib
    export CC=$(exhost --tool-prefix)cc

    MAKE_ARGS="
        ${DEFAULT_SRC_COMPILE_PARAMS[@]}
        MKAUDIT=no
        MKBASHCOMP=$(option bash-completion yes no)
        MKNET=$(option newnet yes no)
        MKPAM=$(optionv pam)
        MKSELINUX=no
        MKSTATICLIBS=no
        MKSYSVINIT=no
        MKZSHCOMP=$(option zsh-completion yes no)
        SH=/bin/bash
    "

    emake ${MAKE_ARGS}
}

src_test() {
    export READELF=$(exhost --tool-prefix)readelf
    default
}

src_install() {
    emake \
        ${DEFAULT_SRC_INSTALL_PARAMS[@]} \
        ${MAKE_ARGS} \
        DESTDIR="${IMAGE}" install

    keepdir /usr/$(exhost --target)/libexec/rc/init.d
    keepdir /usr/$(exhost --target)/libexec/rc/tmp

    insinto /etc/logrotate.d
    newins "${FILES}"/openrc.logrotate openrc

    dodoc ChangeLog *.md
    if option newnet; then
        dodoc README.newnet
    fi
}

pkg_postinst() {
    elog "Since openrc 0.35 openrc has a cgroup init.d script which"
    elog "you'll have to add to your startup if you need cgroups."
}

