# Copyright 2008 Santiago M. Mola
# Copyright 2008, 2009, 2010, 2011 Ingmar Vanhassel
# Copyright 2011 Markus Rothe
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'ghc-6.8.2.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation
# Copyright 2018 Julian Ospald <hasufell@posteo.de>

MY_PN="ghc"
MY_PNV="${MY_PN}-${PV}"
MY_PNVR="${MY_PN}-${PVR}"

require bash-completion flag-o-matic alternatives
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

SUMMARY="The Glorious Glasgow Haskell Compilation System (non-haskell packages version)"
DESCRIPTION="
GHC is a state-of-the-art, open source, compiler and interactive environment for the functional
language Haskell.
"
HOMEPAGE="https://www.haskell.org/ghc/"
DOWNLOADS="
    https://downloads.haskell.org/~ghc/${PV}/${MY_PNV}-src.tar.xz
    https://downloads.haskell.org/~ghc/${PV}/${MY_PNV}-testsuite.tar.xz
    https://downloads.haskell.org/~ghc/${PV}/ghc-${PV}-x86_64-fedora27-linux.tar.xz
"

PLATFORMS="-* ~amd64"

BUGS_TO=""
REMOTE_IDS="freecode:${MY_PN}"
UPSTREAM_RELEASE_NOTES="http://www.haskell.org/${MY_PN}/docs/${PV}/html/users_guide/release-${PV//./-}.html"

LICENCES="BSD-3"
SLOT="${PV}"
MYOPTIONS="
    doc
    llvm [[ description = [ Code generation using LLVM ] ]]
"

# `make test` takes really long, so disable it. It also succeeds (i.e. returned value is zero) in
# case of unexpected failures. There are always unexpected failures.
# If you really want to run the tests, then enable expensive tests in order to run `make fulltest`,
# which runs the same tests as `make test`, but includes more test cases per test.
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-python/Sphinx
    build+run:
        dev-lang/perl:*[>=5.6.1]
        dev-libs/gmp:=[>=5]
        dev-libs/libffi
        llvm? ( dev-lang/llvm[>=2.7] )
        !dev-lang/ghc [[ resolution = uninstall-blocked-before ]]
        !dev-lang/ghc-bin [[ resolution = uninstall-blocked-before ]]
"

WORK=${WORKBASE}/${MY_PNV}

src_unpack() {
    unpack ghc-${PV}-x86_64-fedora27-linux.tar.xz
    edo mv "${WORK}" "${WORK}-pre"
    unpack ${MY_PNV}-src.tar.xz
    unpack ${MY_PNV}-testsuite.tar.xz
}

src_prepare() {
    # prepare pre-built ghc
    edo pushd "${WORK}-pre"
    edo ./configure \
        --prefix="${TEMP}/ghc-pre" \
        --target=$(exhost --target)
    emake install
    edo popd

    autotools_src_prepare
}

src_configure() {
    # Initialize build.mk
    echo '# Exherbo changes' > mk/build.mk

    cat <<EOF >> mk/build.mk
# Put docs into the right place
docdir = /usr/share/doc/${MY_PNVR}
htmldir = /usr/share/doc/${MY_PNVR}/html
EOF

    if option doc; then
        echo "BUILD_SPHINX_HTML=YES" >> mk/build.mk
        echo "BUILD_SPHINX_PDF=NO" >> mk/build.mk
        echo "HADDOCK_DOCS=YES" >> mk/build.mk
    else
        echo "BUILD_SPHINX_HTML=NO" >> mk/build.mk
        echo "BUILD_SPHINX_PDF=NO" >> mk/build.mk
        echo "HADDOCK_DOCS=NO" >> mk/build.mk
    fi

    GHC_SRC_CONFIGURE_OPTIONS+=( --with-ghc=${TEMP}/ghc-pre/bin/ghc )
    export PATH="${WORKBASE}/usr/bin:${PATH}"

    if option llvm; then
        echo "GhcWithLlvmCodeGen=YES" >> mk/build.mk
    else
        echo "GhcWithLlvmCodeGen=NO" >> mk/build.mk
    fi

    GHC_SRC_CONFIGURE_OPTIONS+=(
        --with-system-libffi
        --with-ffi-includes=$(${PKG_CONFIG} --variable includedir libffi)
    )

    # host and build cause compilation (at least for 7.6.1) to fail, as they're intended for
    # cross-compilation.
    econf \
        AR=${AR} \
        CC=${CC} \
        --target=$(exhost --target) \
        "${GHC_SRC_CONFIGURE_OPTIONS[@]}"

    # don't strip anything. Very useful when stage2 SIGSEGVs on you
    echo "STRIP_CMD = :" >> mk/build.mk
}

src_install() {
    default

    # libraries/ is a partial dupe of the html/ dir hierarchy, but
    # containing only LICENSE files
    edo rm -rf "${IMAGE}"/usr/share/doc/${MY_PNVR}/{LICENSE,libraries}

    dobashcompletion "${FILES}/ghc-bash-completion"
    alternatives_for ghc ${SLOT} ${SLOT} \
        /usr/share/bash-completion/completions/ghc-bin{,-${SLOT}}

    # some scripts are not versioned, move
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{hp2ps,hp2ps-${SLOT}}
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{hpc,hpc-${SLOT}}
    edo mv "${IMAGE}"/usr/$(exhost --target)/bin/{hsc2hs,hsc2hs-${SLOT}}

    # remove unversioned symlinks, we create alternatives later
    local ghc_bin
    for ghc_bin in ghc ghci ghc-pkg $(optionq doc && echo "haddock") runghc runhaskell ; do
        edo rm "${IMAGE}"/usr/$(exhost --target)/bin/${ghc_bin}
    done
    unset ghc_bin

    # create alternatives for scripts/binaries
    alternatives_for ghc ${SLOT} ${SLOT} \
        /usr/$(exhost --target)/bin/ghc{,-${SLOT}} \
        /usr/$(exhost --target)/bin/ghci{,-${SLOT}} \
        /usr/$(exhost --target)/bin/ghc-pkg{,-${SLOT}} \
        $(optionq doc &&  echo "/usr/$(exhost --target)/bin/haddock{,-ghc-${SLOT}}") \
        /usr/$(exhost --target)/bin/hp2ps{,-${SLOT}} \
        /usr/$(exhost --target)/bin/hpc{,-${SLOT}} \
        /usr/$(exhost --target)/bin/hsc2hs{,-${SLOT}} \
        /usr/$(exhost --target)/bin/runghc{,-${SLOT}} \
        /usr/$(exhost --target)/bin/runhaskell /usr/$(exhost --target)/bin/runghc-${SLOT}

    # alternatives for manpages
    if [[ -d ${IMAGE}/usr/share/man ]]; then
        local file alternatives=()
        for file in "${IMAGE}"/usr/share/man/*/*; do
            alternatives+=(
            ${file#${IMAGE}}
            ${file#${IMAGE}}-${SLOT}
            )
        done

        alternatives_for \
            ghc ${SLOT} ${SLOT} \
            "${alternatives[@]}"
    fi
}

src_test_expensive() {
    emake fulltest
}

