# Copyright 2017 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require github [ user="evhub" tag="v${PV}" ]
require pypi setup-py [ import=setuptools ]

SUMMARY="Simple, elegant, Pythonic functional programming"
DESCRIPTION="
Coconut is a functional programming language that compiles to Python. Since all
valid Python is valid Coconut, using Coconut will only extend and enhance what
you're already capable of in Python.

Why use Coconut? Coconut is built to be fundamentally useful. Coconut enhances
the repertoire of Python programmers to include the tools of modern functional
programming, in such a way that those tools are easy to use and immensely
powerful; that is, Coconut does to functional programming what Python did to
imperative programming. And Coconut code runs the same on any Python version,
making the Python 2/3 split a thing of the past.
"
HOMEPAGE="http://coconut-lang.org/ ${HOMEPAGE}"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

RESTRICT="test" # missing deps

# python setup.py egg_info
# cat coconut.egg-info/requires.txt
# we ignore upper bounds of:
#  * cPyparsing
#  * mypy
#  * prompt_toolkit[python_abis:2.7]
DEPENDENCIES="
    build+run:
        dev-python/Pygments[>=2.3.1][python_abis:*(-)?]
        dev-python/prompt_toolkit[>=1.0][python_abis:*(-)?]
        dev-python/cPyparsing[>=2.4.5.0.1.1][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/futures[>=3.3][python_abis:2.7]
            dev-python/prompt_toolkit[>=1.0][python_abis:*(-)?]
       )
    suggestion:
        dev-python/mypy[>=0.540] [[ description = [ coconut can run mypy on the compiled python code ] ]]
        dev-python/psutil[>=5] [[ description = [ improves use of the --jobs flag ] ]]
        dev-python/watchdog[>=0.9] [[ description = [ enables use of the --watch flag ] ]]
        python_abis:2.7? (
            dev-python/trollius[>=2.2] [[ description = [ enables use of the asyncio library on Python 2 versions by making use of trollius ] ]]
        )
"

BUGS_TO="hasufell@posteo.de"

prepare_one_multibuild() {
    python_prepare_one_multibuild

    # https://github.com/evhub/coconut/issues/527
    edo iconv -c -f utf-8 -t ascii README.rst > "${TEMP}"/README.rst
    edo $(exhost --tool-prefix)strings "${TEMP}"/README.rst > README.rst
}

