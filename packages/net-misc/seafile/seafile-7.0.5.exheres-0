# Copyright 2018 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2

require python [ blacklist=3 has_bin=true multibuild=false ]
require github [ user=haiwen tag="v${PV}" ]
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]
require vala [ vala_dep=true ]

SUMMARY="File syncing and sharing software with file encryption and group sharing, emphasis on reliability and high performance"
HOMEPAGE="https://www.seafile.com ${HOMEPAGE}"

LICENCES="GPL-2" # has openssl linking exception
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-db/sqlite:3
        dev-libs/glib:2
        dev-libs/jansson
        dev-libs/libevent:=
        net-libs/libsearpc[python_abis:*(-)?]
        net-misc/curl
        sys-libs/zlib
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl?  ( dev-libs/openssl )
    run:
        dev-python/future[python_abis:*(-)?]
"

BUGS_TO="hasufell@posteo.de"

src_prepare() {
    default
    edo sed -i -e 's/valac /${VALAC} /' lib/Makefile.am
    eautoreconf
}

src_install() {
    default
    edo sed -i -e "s:env python[[:digit:].]*$:env ${PYTHON##*/}:" \
        "${IMAGE}"/usr/$(exhost --target)/bin/seaf-cli
}
