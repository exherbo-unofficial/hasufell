# Copyright 2016 Julian Ospald <hasufell@posteo.de>
# Distributed under the terms of the GNU General Public License v2
#
# Based in part upon 'netifrc-0.3.1.ebuild' from Gentoo, which is:
#     Copyright 1999-2015 Gentoo Foundation
#     Distributed under the terms of the GNU General Public License v2

require udev-rules github [ user=gentoo tag=${PV} ]

HOMEPAGE="https://www.gentoo.org/proj/en/base/openrc/"

SUMMARY="Gentoo Network Interface Management Scripts"
LICENCES="BSD-2"
SLOT="0"

PLATFORMS="~amd64 ~x86"

DEPENDENCIES="
    build:
        virtual/pkg-config
    run:
        sys-apps/gentoo-functions
"

src_compile() {
    MAKE_ARGS="
        UDEVDIR=${UDEVRULESDIR}
        LIBEXECDIR=/usr/$(exhost --target)/libexec/${PN}
        PF=${PNVR}"

    emake ${MAKE_ARGS} all
}

src_install() {
    emake ${MAKE_ARGS} DESTDIR="${IMAGE}" install
    dodoc README CREDITS FEATURE-REMOVAL-SCHEDULE STYLE TODO
}

pkg_postinst() {
    if [[ ! -e "${ROOT}"/etc/conf.d/net ]]; then
        elog "The network configuration scripts will use dhcp by"
        elog "default to set up your interfaces."
        elog "If you need to set up something more complete, see"
        elog "${ROOT}/usr/share/doc/${PNVR}/README"
    fi
}

